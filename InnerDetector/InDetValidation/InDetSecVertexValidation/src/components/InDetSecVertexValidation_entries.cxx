/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

// Gaudi/Athena include(s):
//#include "GaudiKernel/DeclareFactoryEntries.h"

// Local include(s):
#include "../PhysValSecVtx.h"

DECLARE_COMPONENT( PhysValSecVtx )