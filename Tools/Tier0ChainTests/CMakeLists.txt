# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

# Declare the package name:
atlas_subdir( Tier0ChainTests )

# Install files from the package:
atlas_install_scripts( test/*.sh )
